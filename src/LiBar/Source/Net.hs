module LiBar.Source.Net where

import qualified Data.Map.Merge.Strict as M
import qualified Data.Map.Strict as M
import LiBar
import LiBar.Types
import Reflex

readNet :: IO (M.Map String [Double])
readNet = do
  values <- drop 2 . lines <$> readFile "/proc/net/dev"
  return . fmap (fmap read) . M.fromList $ [(init . head . words $ x, tail . words $ x) | x <- values]

netLoad :: (LiBarMonad m t c) => m (Dynamic t (M.Map String [Double]))
netLoad = do
  net <- mkEventDef readNet
  stateful go M.empty net
  where
    go :: M.Map String [Double] -> M.Map String [Double] -> M.Map String [Double]
    go = M.merge M.preserveMissing M.preserveMissing $ M.zipWithMatched (const . zipWith . flip $ (-))
