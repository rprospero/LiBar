-- |
-- Module      : LiBar.Source.CPU
-- Description : Sources for the current processor load
-- Copyright   : (c) Adam Washington, 2020
-- Maintainer  : rprospero@gmail.com
-- Stability   : experimental
-- Portability : Linux
--
-- This module exports sources that will give the current processor
-- state.  It currently only works on Linux.
module LiBar.Source.CPU
  ( cpuLoad,
    cpuSum,
    CpuLoad (..),
    readCpu,
  )
where

import Data.Default
import Data.List (isPrefixOf)
import LiBar
import LiBar.Types
import Reflex

data CpuLoad = CpuLoad
  { cpuUser :: Double,
    cpuNice :: Double,
    cpuSystem :: Double,
    cpuIdle :: Double,
    cpuIOWait :: Double,
    cpuIrq :: Double,
    cpuSoftIrq :: Double
  }
  deriving (Show)

instance Default CpuLoad where
  def = CpuLoad 0 0 0 0 0 0 0

diffCpuLoad :: CpuLoad -> CpuLoad -> CpuLoad
diffCpuLoad (CpuLoad a1 a2 a3 a4 a5 a6 a7) (CpuLoad b1 b2 b3 b4 b5 b6 b7) = CpuLoad (a1 - b1) (a2 - b2) (a3 - b3) (a4 - b4) (a5 - b5) (a6 - b6) (a7 - b7)

cpuSum :: CpuLoad -> Double
cpuSum (CpuLoad a b c d e f g) = a + b + c + d + e + f + g

calcCpuLoad :: [Integer] -> CpuLoad
calcCpuLoad cols = CpuLoad (go 0) (go 1) (go 2) (go 3) (go 4) (go 5) (go 6)
  where
    go idx = fromInteger (cols !! idx)

columnsCalc :: String -> [Integer]
columnsCalc = map read . tail . words

readCpu :: String -> [CpuLoad]
readCpu = map (calcCpuLoad . columnsCalc) . takeWhile ("cpu" `isPrefixOf`) . lines

cpuLoad' :: (LiBarMonad m t c) => m (Dynamic t [CpuLoad])
cpuLoad' = do
  result <- mkEvent (replicate 5 def) $ readCpu <$> readFile "/proc/stat"
  stateful (zipWith $ flip diffCpuLoad) (replicate 5 def) result

-- | A source with the current cpu load for processor.  The first
-- element is the complete load across all processors.
cpuLoad :: (LiBarMonad m t c) => m (Dynamic t [CpuLoad])
cpuLoad = cpuLoad'
