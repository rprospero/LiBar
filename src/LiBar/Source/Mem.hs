-- |
-- Module      : LiBar.Source.Mem
-- Description : Sources for the current memory usage
-- Copyright   : (c) Adam Washington, 2020
-- Maintainer  : rprospero@gmail.com
-- Stability   : experimental
-- Portability : Linux
--
-- This module exports sources that will give the current memory
-- state.  It currently only works on Linux.
module LiBar.Source.Mem
  ( memLoad,
    MemLoad (..),
    memAvailableFraction,
    memFreeFraction,
    memUsedFraction,
  )
where

import LiBar.Types

-- | Information about the current memory usage
data MemLoad = MemLoad
  { -- | The total system memory
    memTotal :: Double,
    -- | The amount of memory that could be allocated
    memAvailable :: Double,
    -- | The amount of unused memory
    memFree :: Double
  }
  deriving (Show)

instance Dispable MemLoad

-- | Returns a value between zero and one representing the fraction of
-- memory used by the system, with one representing a machine that is
-- out of memory.
memUsedFraction :: MemLoad -> Double
memUsedFraction x = 1 - memAvailableFraction x

-- | Returns a value between zero and one representing the fraction of
-- memory used by the system, with zero representing a machine that is
-- out of memory.
memAvailableFraction :: MemLoad -> Double
memAvailableFraction x = memAvailable x / memTotal x

-- | Returns a value between zero and one representing the fraction of
-- memory allocated by the system with one representing all memory
-- being allocated.
memFreeFraction :: MemLoad -> Double
memFreeFraction x = memFree x / memTotal x

-- | An action that returns the current memory state of the machine.  Currently only works on Linux.
memLoad :: IO MemLoad
memLoad = do
  params <- lines <$> readFile "/proc/meminfo"
  let values = read . (!! 1) . words <$> params
  return $ MemLoad (head values) (values !! 2) $ values !! 2
