-- |
-- Module      : LiBar.Source.Waka
-- Description : Sources from the WakaTime tracking site.
-- Copyright   : (c) Adam Washington, 2020
-- Maintainer  : rprospero@gmail.com
-- Stability   : experimental
-- Portability : Linux
--
-- This module exports sources that will give your current work status
-- from the WakaTime website.  Note that you'll need to acquire an API
-- key from them to use this functionality.
module LiBar.Source.Waka (WakaSummary (..), WakaInfo (..), getKey) where

import Control.Monad.IO.Class
import Control.Monad.Reader (ReaderT)
import Control.Monad.Reader.Class
import Data.Aeson
import Data.Char (toLower)
import qualified Data.Text as T
import Data.Time.Format
import Data.Time.LocalTime
import GHC.Generics
import GHC.Records
import Network.HTTP.Req

-- | The current effort in a single project
data WakaInfo = WakaInfo
  { -- | The number of hours spent on the projects today.
    wakaInfoHours :: Int,
    -- | The value this work is associated with.  It might be a
    -- project, computer, language, etc.
    wakaInfoName :: Maybe T.Text,
    -- | The number of minutes spent on the project
    wakaInfoMinutes :: Int,
    -- | What percent of the time has been spent on this project
    wakaInfoPercent :: Maybe Double,
    -- | The total number of seconds spent on the project.
    wakaInfoTotal_seconds :: Double
  }
  deriving (Show, Generic)

instance FromJSON WakaInfo where
  parseJSON =
    genericParseJSON $
      defaultOptions
        { fieldLabelModifier = map toLower . drop 8
        }

-- | The user's complete work status
data WakaSummary = WakaSummary
  { -- | The user's work across all projects
    wakaSummaryGrand_total :: WakaInfo,
    -- | The user's individual work for each project.
    wakaSummaryProjects :: [WakaInfo],
    -- | The user's individual work in each language.
    wakaSummaryLanguages :: [WakaInfo],
    -- | The user's individual work on each OS.
    wakaSummaryOperating_systems :: [WakaInfo],
    -- | The user's individual work in each editor.
    wakaSummaryEditors :: [WakaInfo],
    wakaSummaryDependencies :: [WakaInfo],
    -- | The user's individual work on each computer.
    wakaSummaryMachines :: [WakaInfo],
    -- | The user's individual work in each branch.
    wakaSummaryBranches :: Maybe [WakaInfo],
    -- | The user's individual work on each entity.
    wakaSummaryEntities :: Maybe [WakaInfo]
  }
  deriving (Show, Generic)

instance FromJSON WakaSummary where
  parseJSON =
    genericParseJSON $
      defaultOptions
        { fieldLabelModifier = map toLower . drop 11
        }

data WakaResponse = WakaResponse
  { wakaData :: [WakaSummary],
    wakaStart :: ZonedTime,
    wakaEnd :: ZonedTime
  }
  deriving (Show, Generic)

instance FromJSON WakaResponse where
  parseJSON =
    genericParseJSON $
      defaultOptions
        { fieldLabelModifier = map toLower . drop 4
        }

wakaUrl :: T.Text -> ZonedTime -> ZonedTime -> Req (JsonResponse WakaResponse)
wakaUrl key start end =
  req
    GET
    ( https "wakatime.com"
        /: "api"
        /: "v1"
        /: "users"
        /: "current"
        /: "summaries"
    )
    NoReqBody
    jsonResponse
    $ "api_key"
      =: T.init key
      <> "start"
      =: fmt start
      <> "end"
      =: fmt end
  where
    fmt = T.pack . formatTime defaultTimeLocale "%Y-%m-%d"

-- | This action returns all of the Waka summaries associate with the
-- current user.
getKey ::
  (HasField "wakaPassword" config T.Text) => ReaderT config IO [WakaSummary]
getKey = do
  key <- asks (getField @"wakaPassword")
  time <- liftIO getZonedTime
  let url = wakaUrl key time time
  r <- liftIO $ runReq defaultHttpConfig url
  return . wakaData $ responseBody r
