module LiBar.Source.GH where

import Control.Monad.IO.Class
import Control.Monad.Reader (ReaderT)
import Control.Monad.Reader.Class
import Data.Aeson
import Data.Aeson.Types (parseFail, typeMismatch)
import Data.Char (toLower)
import qualified Data.Text as T
import Data.Text.Encoding (encodeUtf8)
import GHC.Generics
import GHC.Records
import LiBar.Types
import Network.HTTP.Req

data GHSubject = GHSubject
  { ghSubjectTitle :: T.Text,
    ghSubjectType :: T.Text
  }
  deriving (Generic, Show)

instance FromJSON GHSubject where
  parseJSON =
    genericParseJSON $
      defaultOptions
        { fieldLabelModifier = map toLower . drop 9
        }

newtype GHRepo = GHRepo
  { ghRepoName :: T.Text
  }
  deriving (Generic, Show)

instance FromJSON GHRepo where
  parseJSON =
    genericParseJSON $
      defaultOptions
        { fieldLabelModifier = map toLower . drop 6
        }

data GHReason = Assign | Author | Comment | Invitation | Manual | Mention | ReviewRequested | SecurityAlert | StateChange | Subscribed | TeamMention
  deriving (Show, Generic, Ord, Eq)

instance FromJSON GHReason where
  parseJSON (String "assign") = return Assign
  parseJSON (String "author") = return Author
  parseJSON (String "comment") = return Comment
  parseJSON (String "invitation") = return Invitation
  parseJSON (String "manual") = return Manual
  parseJSON (String "mention") = return Mention
  parseJSON (String "review_requested") = return ReviewRequested
  parseJSON (String "security_alert") = return SecurityAlert
  parseJSON (String "state_change") = return StateChange
  parseJSON (String "subscribed") = return Subscribed
  parseJSON (String "team_mention") = return TeamMention
  parseJSON (String reason) = parseFail $ "Unknown Github Reason " <> T.unpack reason
  parseJSON invalid = typeMismatch "String" invalid

instance Dispable GHReason where
  display Assign = "\xf01c"
  display Author = "\xf007"
  display Comment = "\xf41b"
  display Invitation = "\xf0e0"
  display Manual = "\xf013"
  display Mention = "\xf67a"
  display ReviewRequested = "\xe726"
  display SecurityAlert = "\xf982"
  display StateChange = "\xe29a"
  display Subscribed = "\xf41b"
  display TeamMention = "\xf0c0"

data GHNote = GHNote
  { ghNoteReason :: GHReason,
    ghNoteSubject :: GHSubject,
    ghNoteRepository :: GHRepo
  }
  deriving (Generic, Show)

instance FromJSON GHNote where
  parseJSON =
    genericParseJSON $
      defaultOptions
        { fieldLabelModifier = map toLower . drop 6
        }

ghNoteUrl :: T.Text -> T.Text -> Req (JsonResponse [GHNote])
ghNoteUrl user pass = req GET (https "api.github.com" /: "notifications") NoReqBody jsonResponse $ basicAuth (encodeUtf8 user) (encodeUtf8 pass) <> header "User-Agent" (encodeUtf8 user)

getGh :: (HasField "ghUser" config T.Text, HasField "ghPassword" config T.Text) => ReaderT config IO [GHNote]
getGh = do
  user <- asks (getField @"ghUser")
  pass <- asks (getField @"ghPassword")
  r <- liftIO $ runReq defaultHttpConfig $ ghNoteUrl user pass
  return $ responseBody r
