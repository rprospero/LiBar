-- |
-- Module      : LiBar.Source.Time
-- Description : Sources for the current time
-- Copyright   : (c) Adam Washington, 2020
-- Maintainer  : rprospero@gmail.com
-- Stability   : experimental
-- Portability : Universal
--
-- This module exports sources that will give the current date and
-- time.
module LiBar.Source.Time where

import Data.Text
import Data.Time.Format
import Data.Time.LocalTime

-- | Get the current time, formatted as a string.
getTime ::
  -- | A format string for the current time.  "%x %T" will give the
  -- date and time.  The full format string descritption is in
  -- 'Data.Time.Format.formatTime'
  Text ->
  IO Text
getTime fmt = pack . formatTime defaultTimeLocale (unpack fmt) <$> getZonedTime
