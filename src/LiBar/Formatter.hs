{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE RankNTypes #-}

-- |
-- Module      : LiBar.Formatter
-- Description : Sources for the current memory usage
-- Copyright   : (c) Adam Washington, 2020
-- Maintainer  : rprospero@gmail.com
-- Stability   : experimental
-- Portability : Linux
--
-- This module exports a bunch of utility functions for formatting the
-- text in the status bar.  Using these formatting functions will
-- allow the result to be portable across multiple different status
-- bars, so that you can change your front-end without needing to
-- rewrite your whole setup.
module LiBar.Formatter
  ( Formatter (..),
    afixBar,
    barCycle,
    crop,
    rightBarCycle,
    condRecolor,
    marked,
    tripartite,
    vbar,
  )
where

import Data.List (foldl')
import Data.Text (Text, length)
import LiBar.Types
import Prelude hiding (length)

-- | The general class for handling different status bars
class Formatter x where
  -- | Print text on the left side of the bar
  barLeft :: forall a. x -> Displayer a

  -- | Print text in the center of the bar
  barCenter :: forall a. x -> Displayer a

  -- | Print text on the right side of the bar
  barRight :: forall a. x -> Displayer a

  -- | Change the text color
  recolor :: forall a. x -> Color -> Displayer a

  -- | The default color for text in the bar
  defaultColor :: x -> Color

  -- | A set of prefered highlight colors for text
  highlights :: x -> [Color]

-- | Applies a series of display modifiers in order onto a value.
-- This is usually what you will want to do to convert any values into
-- text to be displayed
afixBar :: (Dispable a) => [Displayer a] -> a -> Text
afixBar fs value = foldl' go (display value) fs
  where
    go old disp = disp value old

arrow :: (Formatter f) => f -> Color -> Displayer a
arrow f c value old = recolor f c value "\xE0B6" <> old

rightarrow :: (Formatter f) => f -> Color -> Displayer a
rightarrow f c value old = old <> recolor f c value "\xE0B4"

-- | Display a list of values across a rainbow of backgrounds,
-- separated by left pointing arrows
barCycle :: (Dispable a, Formatter x) => x -> [a] -> Text
barCycle f bars =
  let cs = cycle $ highlights f
      dflt = defaultColor f
      go ((c, cn), bar) acc = afixBar [recolor f cn, arrow f (snd cn, snd c)] bar <> acc
   in foldr go "" $ zip (zip (dflt : cs) cs) bars

-- | Display a list of values across a rainbow of backgrounds,
-- separated by right pointing arrows
rightBarCycle :: (Dispable a, Formatter f) => f -> [a] -> Text
rightBarCycle f bars =
  let cs = cycle $ highlights f
      dflt = defaultColor f
      go ((c, cn), bar) acc = acc <> afixBar [recolor f cn, rightarrow f (snd cn, snd c)] bar
   in foldr go "" $ zip (zip (dflt : cs) cs) $ reverse bars

-- | Change the color to the text only when a certain condition is
-- met.  For example, turning the cpu status bar red when the system
-- load exceeds 90%.
condRecolor :: (Formatter f) => f -> (a -> Bool) -> Color -> Displayer a
condRecolor f cond c =
  let rc = recolor f c
      result value old =
        if cond value then rc value old else old
   in result

-- | Add a header message to a value.  This is useful for adding
-- labels to values in the bar.
marked :: Text -> Displayer a
marked mark _ old = mark <> old

-- | Turn a fractional value into a unicode vertical bar.  Zero is
-- considered empty and one is considered full.
vbar :: (Fractional a, Ord a) => Displayer a
vbar value _
  | value < 0 = " "
  | value < (1 / 8) = "▁"
  | value < (2 / 8) = "▂"
  | value < (3 / 8) = "▃"
  | value < (4 / 8) = "▄"
  | value < (5 / 8) = "▅"
  | value < (6 / 8) = "▆"
  | value < (7 / 8) = "▇"
  | otherwise = "█"

-- | Create a bar with three distinct sections
tripartite :: (Formatter f, Dispable a) => f -> a -> a -> a -> Text
tripartite f left center right =
  afixBar [barLeft f] left <> afixBar [barCenter f] center <> afixBar [barRight f] right

-- | Intelligently drop list elements until the total string has no
-- more than the specified number of characters
crop :: (Dispable a) => Int -> [a] -> [a]
crop _ [] = []
crop c x = map fst . takeWhile ((<= c) . snd) . zip x . tail $ scanl (+) 0 $ map (length . display) x
