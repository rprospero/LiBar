{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

-- |
-- Module      : LiBar.Sink.Dzen
-- Description : A formatter for the LemonBar status bar
-- Copyright   : (c) Adam Washington, 2020
-- Maintainer  : rprospero@gmail.com
-- Stability   : experimental
-- Portability : Linux
--
-- This module exports a formatter for displaying the output in the
-- LemonBar status bar.
module LiBar.Sink.Dzen (Dzen (..)) where

import Data.Default
import LiBar.Formatter
import LiBar.Types

-- | A formatter that displays the bar in LemonBar
newtype Dzen = Dzen
  { -- | The color scheme to use in the bar.
    dzenScheme :: ColorScheme
  }

instance Formatter Dzen where
  barLeft _ = marked "^pa(_LEFT)"
  barCenter _ = marked "^p(_CENTER)"
  barRight _ = marked "^p(_RIGHT)"
  defaultColor = csDefaultColor . dzenScheme
  highlights = csHighlights . dzenScheme
  recolor _ color _ old =
    let wrapper _ Nothing = ("", "")
        wrapper tag (Just c) =
          let name = "^" <> tag <> "g(#"
           in (name <> c <> ")", name <> ")")
        outer = wrapper "f" $ fst color
        inner = wrapper "b" $ snd color
     in fst outer <> fst inner <> old <> snd inner <> snd outer

instance Default Dzen where
  def =
    Dzen $
      ColorScheme (Nothing, pure "1c433c") $
        map
          (\x -> (pure "1c433c", pure x))
          [ "f7a8a5",
            "e8cbba",
            "fcd88a",
            "b4c8a1",
            "6ec0ff",
            "bd94ca",
            "8e725b"
          ]
