{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

-- |
-- Module      : LiBar.Sink.LemonBar
-- Description : A formatter for the LemonBar status bar
-- Copyright   : (c) Adam Washington, 2020
-- Maintainer  : rprospero@gmail.com
-- Stability   : experimental
-- Portability : Linux
--
-- This module exports a formatter for displaying the output in the
-- LemonBar status bar.
module LiBar.Sink.LemonBar (LemonBar (..)) where

import Data.Default
import LiBar.Formatter
import LiBar.Types

-- | A formatter that displays the bar in LemonBar
newtype LemonBar = LemonBar
  { -- | The color scheme to use in the bar.
    lbScheme :: ColorScheme
  }

instance Formatter LemonBar where
  barLeft _ = marked "%{l}"
  barCenter _ = marked "%{c}"
  barRight _ = marked "%{r}"
  defaultColor = csDefaultColor . lbScheme
  highlights = csHighlights . lbScheme
  recolor _ c = \_ old ->
    let outer = wrapper "F" $ fst c
        inner = wrapper "B" $ snd c
     in fst outer <> fst inner <> old <> snd inner <> snd outer
    where
      wrapper _ Nothing = ("", "")
      wrapper tag (Just color) =
        ("%{" <> tag <> "#" <> color <> "}", "%{" <> tag <> "-}")

instance Default LemonBar where
  def =
    LemonBar $
      ColorScheme (Nothing, pure "1c433c") $
        map
          (\x -> (pure "1c433c", pure x))
          [ "f7a8a5",
            "e8cbba",
            "fcd88a",
            "b4c8a1",
            "6ec0ff",
            "bd94ca",
            "8e725b",
            "8e5b5e"
          ]
