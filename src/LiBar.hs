-- |
-- Module      : LiBar
-- Description : A framework for better status bars
-- Copyright   : (c) Adam Washington, 2020
-- Maintainer  : rprospero@gmail.com
-- Stability   : experimental
-- Portability : Universal
--
-- LiBar is a framework for populating status bars (such as lemonbar
-- or dzen). The hypothesis is that providing modularity and
-- composability will enable greater flexability and portability.
module LiBar where

import Control.Concurrent
import qualified Control.Monad as M
import Control.Monad.IO.Class
import Control.Monad.Reader
import Data.Default
import Data.Functor (($>))
import Data.Text (Text)
import Data.Text.IO (putStrLn)
import LiBar.Types
import Prelude hiding (putStrLn)
import Reflex
import Reflex.Host.Headless (runHeadlessApp)
import System.IO hiding (putStrLn)

-- | Create a Dynamic value that is powered by the result of an underlying action
mkEvent ::
  (LiBarMonad m t c) =>
  -- | The default value before the action has returned
  a ->
  -- | The action to sample
  IO a ->
  -- | a Dynamic value based on the action's current result
  m (Dynamic t a)
mkEvent base act = mdo
  build <- getPostBuild
  self <-
    holdDyn base =<< throttle 1.0
      =<< performEventAsync
        ( leftmost [void . updated $ self, build] $> \fn -> M.void . liftIO . forkIO $ fn =<< act
        )
  return self

-- | A helper function over 'mkEvent' that uses the default value for
-- the initial value.  Unless the type doesn't have a 'Data.Default'
-- value or you have something very specific in mind, this is probably
-- the function that you want to use.
mkEventDef ::
  (LiBarMonad m t c, Default a) =>
  IO a ->
  m (Dynamic t a)
mkEventDef = mkEvent def

-- | Create a Dynamic value from an action in a reader monad
mkEventReader ::
  (LiBarMonad m t config, Default a) =>
  -- | The action to sample
  ReaderT config IO a ->
  -- | a Dynamic value based on the action's current result
  m (Dynamic t a)
mkEventReader act = do
  config <- ask
  mkEventDef $ runReaderT act config

-- | Fold over the state of an old dynamic to create a new one
stateful ::
  (LiBarMonad m t config) =>
  -- | A function that take the old and new states and returns the new output.
  (b -> b -> c) ->
  -- | An initial output until the second state arrives
  c ->
  -- | A dynamic state
  Dynamic t b ->
  m (Dynamic t c)
stateful f start dyn = do
  dyn' <- scanDyn (,start) (\new (old, _) -> (new, f old new)) dyn
  return $ snd <$> dyn'

-- | Run the LiBar on the console
libar ::
  -- | The network to run
  config ->
  ( forall t m.
    (LiBarMonad m t config) =>
    m (Event t Text, Event t ())
  ) ->
  IO ()
libar config bar = do
  hSetBuffering stdout LineBuffering
  runHeadlessApp $
    flip runReaderT config $ do
      (output, quit) <- bar
      performEvent_ $ liftIO . putStrLn <$> output
      return quit
