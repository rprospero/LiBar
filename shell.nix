{ pkgs ? import <nixpkgs> { } }:

let

in pkgs.mkShell {
  buildInputs = [
    pkgs.cabal-install
    (pkgs.haskellPackages.ghcWithHoogle (pkgs: [
      pkgs.Cabal
      pkgs.aeson
      pkgs.brittany
      pkgs.hlint
      pkgs.req
      pkgs.reflex
      pkgs.haskell-language-server
    ]))
  ];
}
